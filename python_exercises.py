"""Difference between list and iterator."""
# 1) Lists are datastructures just to store objests.
# 2) both list and iterators are iterable

lst = [1, 2, 3, 4, 5]
# list, tuple and dictionary are iterable which means
#  __iter__() methods is implemented in these data structures
lstIter = lst.__iter__()

# We iterate through the iterator by using next() function.or __next__()method.
next(lstIter)

# unzip the zip.
zipLst = list(zip(["a", "b", "c", "d"], [1, 2, 3, 4]))
unzipLst = list(zip(*zipLst))
lst1, lst2 = [i for i, j in zipLst], [j for i, j in zipLst]

# Anonymous functions: lambda
# lambda is single expression not statement


def sum(x, y, z):
    """Return sum of all arguments."""
    return x + y + z


sum(5, 6, 8)

# lambda function
(lambda x, y, z: x + y + z)(5, 6, 8)
list(map(lambda x: x**2, [2, 3, 4, 5]))


def myOperation(fun, a, b):
    """Return result according to fun."""
    return fun(a, b)


myOperation(lambda op1, op2: op1 + op2, 15, 20)
myOperation(lambda op1, op2: op1 - op2, 15, 20)
myOperation(lambda op1, op2: op1 * op2, 15, 20)
