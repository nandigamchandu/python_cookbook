"""Iterators and Generators."""
import itertools
import os
import re
import collections
import heapq

# problem 4.1:
# you need to process items in an iterable, but for whatever reason,
# you can't or don't want to use a for loop.

sample_iter = iter([1, 2, 3, 4, 5, 6, 7, 8])

# my solution:
while True:
    try:
        print(sample_iter.__next__())
    except StopIteration:
        break
# soltion from text book
while True:
    value = next(sample_iter, None)
    if not value:
        break
    print(value)


# Problem 4.2:
# you have built a custom container object that internaly holds a
# list, tuple, or some other iterable. you would like to make iteration
# work with your new container.

class MyContainer:
    """Implement mycontainer class."""

    def __init__(self):
        """Intialize iterator."""
        self.myIter = list()

    def addValue(self, value):
        """To append value to list."""
        self.myIter.append(value)

    def __iter__(self):
        """Return iterator."""
        return self.myIter.__iter__()


myIter = MyContainer()
myIter.addValue(5)
myIter.addValue(6)
myIter.addValue(9)

for i in myIter:
    print(i)


# Problem 4.3:
# You want to implement a custom iteration pattern that's different than
# the usual built-in functions(e.g range(), reversed() etc).


def myReversed(element):
    """Return a reverse iterator."""
    for i in range(len(element)-1, -1, -1):
        yield element[i]


list(myReversed([1, 2, 3, 4, 5, 6]))

# problem 4.4
#  You are building custom object on which you would like to support iteration,
# but world like an easy way to implement the iterator protocol.


class MyIterator:
    """Implement  MyIterator."""

    def __init__(self, value):
        """Initialize value, lastValue and next."""
        self.value = value
        self.lastValue = self
        self.next = None

    def addElement(self, value):
        """To append value."""
        self.lastValue.next = MyIterator(value)
        self.lastValue = self.lastValue.next

    def __next__(self):
        """Get next."""
        value = self.value
        if self.next:
            self.value = self.next.value
            self.next = self.next.next
        else:
            raise(StopIteration)
        return value

    def __iter__(self):
        """Get iter."""
        return self


test = MyIterator(5)
test.addElement(8)
test.addElement(10)
test.addElement(12)
test.addElement(13)
test.addElement(14)
for i in test:
    print(i)


# problem 4.5
# You want to iterate in reverse over a sequence
def myReversed(element):
    """Return a reverse iterator."""
    for i in range(len(element)-1, -1, -1):
        yield element[i]


myRev = myReversed([1, 2, 3, 4, 5, 6])
for i in myRev:
    print(i)


# problem 4.6
# You would like to define a generator function, but it invovles
# extra state that you would like to expose to the user somehow.

# problem 4.7 you want to take a slice of data produced by an iterator,
# but the normal slicing operator doesn't work.

# answer)
class MyIterator:
    """Implement my iterator."""

    def __init__(self, seq):
        """To initialize seq variable."""
        self.seq = seq

    def __getitem__(self, index):
        """Return list from starting index upto ending index."""
        return list(itertools.islice(self.seq, index.start, index.stop,
                                     index.step))


mySeq = MyIterator(iter([1, 2, 3, 4, 5, 6]))
mySeq[1:5]

# problem 4.8
# You want to iterate over items in an iterable, but
# the first few items aren't of interest and you just want to discard them.


# my answer:
def myDropWhile(lst):
    """Retrun list after removal of first unwanted elements."""
    count = 0
    for i in lst:
        if type(i) != str:
            break
        count += 1
    return lst[count:]


lst = ["a", "b", "c", 1, 2, 3, 'a', 4, 5]
myDropWhile(lst)

# text book answer:
list(itertools.dropwhile(lambda x: type(x) == str,  lst))
lst = [1, 2, 3, 4, 5]
a, b, c, *_ = lst

# problem 4.9:
# you want to iterate over all of the possible combinations of
# permutations of a collection of items


# Answer:
# combination.
list(itertools.combinations(range(4), 3))
# permutation.
list(itertools.permutations(range(4), 3))

# problem 4.10:
#  You want to iterate over a sequence, but would like to keep
# track of which element of the sequence is currently being processed.

lst = [2, 8, 11, 13, 14, 15]
# Answers:
for i in range(len(lst)):
    print("Index position {} and its value {}".format(i, lst[i]))

for index, value in enumerate(lst):
    print("Index position {} and its value {}".format(index, value))

# problem 4.11:
# You want to iterate over the items contained in
# more than one sequence at a time

# Answer:
lst1 = (1, 2, 3, 4, 5, 6)
lst2 = ["A", "B", "C", "D", "E", "F"]
for i, j in zip(lst1, lst2):
    print("{} from lst1, {} from lst2".format(i, j))

for i in itertools.zip_longest(lst1, lst2):
    print("{} from lst1, {} from lst2".format(*i))


# problem 4.12:
# You need to perform the same operation on many objects,
# but the objects are contained in different containers, and you'd  like to
# avoid nested loops without losing the readability.


def myFunction(func, *lsts):
    """Return list after applying func on objects of different containers."""
    result = []
    for i in lsts:
        result = result + list(map(func, i))
    return result


myFunction(lambda x: x ** 2, [1, 2, 3], [4, 5, 6], [6, 7, 8])

for i in itertools.chain([1, 2, 3], [4, 5, 6], [6, 7, 8]):
    print((lambda x: x ** 2)(i))


# problem 4.13:
# you want to process data iteratively in the style of a data processing
# pipeline. For instance, you have a huge amount of data that needs to be
# processed, but it con't fit entirely into memory.
def get_opener(filepat):
    """Return file paths which are match with given pattern."""
    for path, dirlist, filelist in os.walk('.'):
        for name in filelist:
            if re.match(filepat, name):
                yield os.path.join(path, name)


def gen_opener(filenames):
    """Open a sequence of files one at a time producing a file object."""
    for filename in filenames:
        f = open(filename, 'rt')
        yield f
        f.close()


files = get_opener('[\w\d]*\.txt$')
textlines = gen_opener(files)
for i in textlines:
    print(i.readlines())


# problem 4.14
def flatten(items, result=None):
    """Return flat list of nested list."""
    if not result:
        result = list()
    ignore_type = (str, bytes)
    for i in items:
        if (isinstance(i, collections.Iterable) and not isinstance(
                                                          i, ignore_type)):
            result = flatten(i, result)
        else:
            result.append(i)
    return result


flatten([1, 2, 3, [5, 6, 8], [5, 5], 'asd', 4])


# problem 4.15:
def mergeSort(*args):
    """Return sorted list."""
    result = sorted(flatten([args]))
    return result


mergeSort([1, 2, 3], [5, 9, 8])
list(heapq.merge([1, 2, 3], [5, 9, 8]))
